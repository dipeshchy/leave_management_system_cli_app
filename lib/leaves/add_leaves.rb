class AddLeave

    # method to add leave for an employee
    def self.add_leave(con)
        puts ""
        puts "Select the user to add leave"
        puts "-----------------------------------"
        puts ""
        begin 
            users = con.query("SELECT * FROM users")
            users_list = Array.new
            if users.size == 0
                puts "List is empty."
            else
                users.each do |u|
                    puts u['fullname']
                    users_list.push(u['fullname'])
                end
                puts "---------------------------"
            end
            puts "Enter the user to add leave from the above list ==>"
            user_to_add_leave = gets.chomp 
            if users_list.include?(user_to_add_leave)

                 # TO check if employees have remaining leaves left or  not
                get_user = con.query("SELECT * FROM users WHERE fullname='#{user_to_add_leave}' LIMIT 1 ")
                unless get_user.size == 0
                    get_user.each do |u|
                        @user_id = u['id']
                        @remaining_leave = u['leaves_left']
                    end
                else
                    puts "User not found"
                end
        
                if @remaining_leave > 0
                    puts "+++++++ ADD LEAVE For #{user_to_add_leave} +++++++++"
                    puts "Please put date in the format of Day-Monthname-Year (like 19 September 2019)"
                    puts "Enter the number of leave days ==>"
                    leave_count = gets.chomp.to_i
                    # check if user leave count is greater than remaining leave or not
                    if @remaining_leave > leave_count
                        puts "Enter Leave Start date ==>"
                        leave_start_date = gets.chomp
                        if leave_count > 1
                            puts "Enter Leave End date ==>"
                            leave_end_date = gets.chomp
                        else
                            leave_end_date = leave_start_date
                        end
                        begin 
                            # TO update leaves numbers,first get the current days and then update with new number of days
                            current_leaves = con.query("SELECT * FROM users WHERE fullname='#{user_to_add_leave}' LIMIT 1 ")
                            unless current_leaves.size == 0
                                current_leaves.each do |l|
                                    @current_leaves_left_count = l['leaves_left'] - leave_count
                                    @current_leaves_taken_count =  l['total_leaves_taken'] + leave_count
                                end
                            else
                                puts "User not found"
                            end
                            con.query("UPDATE users SET leaves_left = #{@current_leaves_left_count} WHERE fullname='#{user_to_add_leave}' ")
                            con.query("UPDATE users SET total_leaves_taken=#{@current_leaves_taken_count} WHERE fullname='#{user_to_add_leave}' ")
                            con.query("INSERT INTO leaves(user_id,leave_count,leave_start_date,leave_end_date) VALUES(#{@user_id},#{leave_count},'#{leave_start_date}','#{leave_end_date}') ")
                            puts "Leave added Successfully."
                        rescue => e 
                            puts e.message
                        end
                    else
                        puts "You have only #{@remaining_leave} days of leaves left. So you cant take #{leave_count} days leave.Do you want to try again?(Y to continue)"
                        usr_choice = gets.chomp.downcase
                        if usr_choice == 'y'
                            add_leave(con)
                        end
                    end
        
                else
                    puts "Sorry. You cant take leave.You already have taken 15 days leave."
                end
                
            else
                puts "User not found.Do you want to try again?(Y to continue)"
                u_choice = gets.chomp.downcase
                if u_choice == 'y'
                    add_leave(con)
                end
            end
        rescue => e 
            puts e.message
        end

    end

end