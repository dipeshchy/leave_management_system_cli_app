class DeleteLeave

    # methodto delete leave,first it lists user whose leave is to delete
    def self.delete_leave(con)
        puts ""
        puts "++++++++++++ DELETE LEAVE ++++++++++++++"
        puts "Enter the name of the user to delete leave from the list below"
        puts "-----------------------------------"
        puts ""
        begin 
            users = con.query("SELECT * FROM users")
            users_list = Array.new
            if users.size == 0
                puts "List is empty."
            else
                users.each do |u|
                    puts u['fullname']
                    users_list.push(u['fullname'])
                end
                puts "---------------------------"
            end
            puts "Select user from the above ==>"
            user_input = gets.chomp 
            if users_list.include?(user_input)
                user_id = 0
                # get current leaves
                current_leave_left = 0
                current_leaves_taken = 0

                get_user = con.query("SELECT * FROM users WHERE fullname='#{user_input}' LIMIT 1 ")
                puts "-----------------------------------------"
                get_user.each do |user|
                    user_id = user['id']
                    current_leave_left = user['leaves_left']
                    current_leaves_taken = user['total_leaves_taken']
                end
                get_leaves = con.query("SELECT * FROM leaves WHERE user_id='#{user_id}' ")
                leave_id_list =[]
                unless get_leaves.size == 0 
                    puts "---------------------------------------"
                    puts "All leaves of #{user_input} "
                    puts "----------------------------------------"
                    puts "| Leave ID | Total Leaves Days | Leave Start Date | Leave End Date |"
                    puts "---------------------------------------------------------"
                    get_leaves.each do |leave|
                        leave_id_list.push(leave['id'])
                        print "| #{leave['id']} \t \t| #{leave['leave_count']} \t \t | #{leave['leave_start_date']} \t | #{leave['leave_end_date']} \t |"
                        puts ""
                        puts "------------------------------------------------------"
                    end
                    puts "Enter the Leave ID ==> "
                    leave_id_to_del = gets.chomp.to_i 
                    if leave_id_list.include?leave_id_to_del
                        leave_del_query = con.query("DELETE FROM leaves where id=#{leave_id_to_del} ")
                        puts "Leave Deleted!!"
                        puts "Do you want to Continue delete others leave?(Y to continue)"
                        u_choice = gets.chomp.downcase
                        if u_choice == 'y'
                            delete_leave(con)
                        end 
                    else
                        puts "Leave ID not found.Do you want to try again?(Y to continue)"
                        u_choice = gets.chomp.downcase
                        if u_choice == 'y'
                            delete_leave(con)
                        end
                    end
                else 
                    puts "No Leaves for #{user_input} yet."
                end
            else
                puts "User not found.Do you want to try again?(Y to continue)"
                u_choice = gets.chomp.downcase
                if u_choice == 'y'
                    delete_leave(con)
                end
            end
        rescue => e 
            puts e.message
        end
    end

end