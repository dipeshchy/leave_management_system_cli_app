class Show_Individual_User_Leave

    # method to show leaves of individual leaves
    def self.show_individual_leaves(con)
        puts ""
        puts "+++++++++ Individual Leave +++++++"
        puts "Select the user to view his/her leave"
        puts "-----------------------------------"
        puts ""
        begin 
            users = con.query("SELECT * FROM users")
            users_list = Array.new
            if users.size == 0
                puts "List is empty."
            else
                users.each do |u|
                    puts u['fullname']
                    users_list.push(u['fullname'])
                end
                puts "---------------------------"
            end
            puts "Select user from the above ==>"
            user_input = gets.chomp 
            if users_list.include?(user_input)
                user_id = nil
                get_user = con.query("SELECT * FROM users WHERE fullname='#{user_input}' LIMIT 1 ")
                puts "-----------------------------------------"
                get_user.each do |user|
                    user_id = user['id']
                    puts "#{user_input} has taken #{user['total_leaves_taken']} days of leaves."
                    puts "#{user_input} has #{user['leaves_left']} days leaves left."
                end
                get_leaves = con.query("SELECT * FROM leaves WHERE user_id='#{user_id}' ")
                unless get_leaves.size == 0 
                    puts "---------------------------------------"
                    puts "All leaves of #{user_input} "
                    puts "----------------------------------------"
                    puts "| Total Leaves Days | Leave Start Date | Leave End Date |"
                    puts "---------------------------------------------------------"
                    get_leaves.each do |leave|
                        print "| #{leave['leave_count']} \t \t | #{leave['leave_start_date']} \t | #{leave['leave_end_date']} \t |"
                        puts ""
                        puts "------------------------------------------------------"
                    end
                else 
                    puts "No Leaves for #{user_input} yet."
                end
            else
                puts "User not found.Do you want to try again?(Y to continue)"
                u_choice = gets.chomp.downcase
                if u_choice == 'y'
                    show_individual_leaves(con)
                end
            end
        rescue => e 
            puts e.message
        end
    end

end