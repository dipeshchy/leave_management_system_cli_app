class ShowLeaves

    # show all leaves of all users
    def self.show_all_leaves(con)
        puts ""
        puts "++++++++ LIST OF ALL LEAVES +++++++ "
        puts ""
        begin
            leaves = con.query("SELECT * FROM leaves INNER JOIN users ON leaves.user_id=users.id")
            unless leaves.size == 0
                puts "-------------------------------------------------------------"
                puts "| Fullname | Leave Count | Leave Start Date | Leave End Date |"
                puts "-------------------------------------------------------------"
                leaves.each do |leave|
                    print "| #{leave['fullname']} | #{leave['leave_count']} \t \t | #{leave['leave_start_date']} | #{leave['leave_end_date']} |"
                    puts ""
                    puts "----------------------------------------------------------"
                end
            else
                puts "List is empty."
            end
        rescue => e 
            puts "Error listing"
            puts e.message
        end
    end

end