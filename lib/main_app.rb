require_relative 'db_connection.rb'

require_relative 'users/add_user.rb'
require_relative 'users/show_users.rb'
require_relative 'users/delete_user.rb'
require_relative 'users/update_user.rb'

require_relative 'leaves/add_leaves.rb'
require_relative 'leaves/show_leaves.rb'
require_relative 'leaves/delete_leave.rb'
require_relative 'leaves/update_leave.rb'
require_relative 'leaves/show_ind_user_leaves.rb'

class Main_Application

    # method to initialize connection
    def initialize
        @@con = Connection.get_connection
        puts ""
        puts "-----------------------------------------------------------"
        puts "++++++++++ WELCOME TO LEAVE MANAGEMENT SYSTEM +++++++++++++"
        puts "-----------------------------------------------------------"
        puts ""
    end

    # Method to show main menu ang get user input
    def main_menu
        puts ""
        puts "------------------------"
        puts "\tMain Menu\t"
        puts "------------------------"
        puts "1. Show All Users"
        puts "2. Add User"
        puts "3. Delete User"
        puts "4. Update User"
        puts "5. Add Leave"
        puts "6. Update Leave"
        puts "7. Show all Leaves"
        puts "8. Show leaves of individual user"
        puts "9. Delete Leave"
        puts "10.Exit"
        puts "-------------------------"
        puts "Choose any of the above(1/2/3/4/5/6/7/8/9/10) ==> "
        menu_choice = gets.chomp
        if menu_choice.match?(/\d/)
            main_menu_handler(menu_choice.to_i)
        else
            puts "Enter number only.String not allowed"
            main_menu
        end
    end

    # method to handle and divert user menu choice
    def main_menu_handler(menu_choice)
        case menu_choice
        when 1
            ShowUser.show_users(@@con)
        when 2 
            AddUser.add_user(@@con)
        when 3
            DeleteUser.delete_user(@@con)
        when 4
            UpdateUser.update_user(@@con)
        when 5
            AddLeave.add_leave(@@con)
        when 6
            UpdateLeave.update_leave(@@con)
        when 7
            ShowLeaves.show_all_leaves(@@con)
        when 8
            Show_Individual_User_Leave.show_individual_leaves(@@con)
        when 9
            DeleteLeave.delete_leave(@@con)
        when 10
            puts "Good Bye!! Thank You!!"
            Connection.close_connection
            exit
        else
            puts "Invalid Input. Choose from (1/2/3/4/5)"
        end
        main_menu
    end

end