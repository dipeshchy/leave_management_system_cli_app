class DeleteUser

    # Method to delete user from the table
    def self.delete_user(con)
        puts ""
        puts "++++++++++ DELETE USER ++++++++++"
        puts "Delete User from the following list"
        puts "-----------------------------------"
        puts ""
        begin 
            users = con.query("SELECT * FROM users")
            users_list = Array.new
            if users.size == 0
                puts "List is empty."
            else
                users.each do |u|
                    puts u['fullname']
                    users_list.push(u['fullname'])
                end
                puts "---------------------------"
            end
            puts "Enter the user to delete from the above list ==>"
            user_del = gets.chomp 
            if users_list.include?(user_del)
                user_delete_query = con.query("DELETE FROM users WHERE fullname='#{user_del}' ")
                puts "User deleted!!"
            else
                puts "User not found.Do you want to try again?(Y to continue,Anything to exit)"
                u_choice = gets.chomp.downcase
                if u_choice == 'y'
                    delete_user(con)
                end
            end
        rescue => e 
            puts e.message
        end
    end

end