class UpdateUser

    # method to update user
    def self.update_user(con)
        puts "++++++++++ UPDATE USER ++++++++++"
        puts "Update User from the following list"
        puts "-----------------------------------"
        begin 
            users = con.query("SELECT * FROM users")
            users_list = Array.new
            if users.size == 0
                puts "List is empty."
            else
                users.each do |u|
                    puts u['fullname']
                    users_list.push(u['fullname'])
                end
                puts "---------------------------"
            end
            puts "Enter the user to update from the above list ==>"
            user_update = gets.chomp 
            if users_list.include?(user_update)
                getuser_query = con.query("SELECT * FROM users WHERE fullname='#{user_update}' LIMIT 1 ")
               
                puts "++++ Fill Up the following credentials +++++"
                puts "Enter Fullname ==>"
                fullname = gets.chomp
                puts "Enter Email ==>"
                email = gets.chomp
                puts "Enter Address ==>"
                address = gets.chomp
                puts "Enter Phone number ==>"
                phone = gets.chomp
                begin
                    con.query("UPDATE users SET fullname='#{fullname}',email='#{email}',address='#{address}',phone='#{phone}' WHERE fullname='#{user_update}'  ")
                    puts "User updated successfully !!"
                rescue => e
                puts "Error updating user."
                puts e.message 
                end

            else
                puts "User not found.Do you want to try again?(Y to continue)"
                u_choice = gets.chomp.downcase
                if u_choice == 'y'
                    update_user(con)
                end
            end
        rescue => e 
            puts e.message
        end
    end

end