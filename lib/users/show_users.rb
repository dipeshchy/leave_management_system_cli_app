class ShowUser

    # method to list all users
    def self.show_users(con)
        puts ""
        puts "----------------------------"
        puts "List of all Users"
        puts "------------------------------"
        puts ""
        begin
            users = con.query("SELECT * FROM users")
            if users.size == 0
                puts "List is empty."
            else
                puts "-----------------------------------------------------------------------------------------------"
                puts "| Fullname \t | Email \t | Address \t | Phone \t | Leaves taken | Leaves left \t |"
                puts "-----------------------------------------------------------------------------------------------"
                users.each do |user|
                    print "| #{user['fullname']} \t | #{user['email']} | #{user['address']} \t | #{user['phone']} \t | #{user['total_leaves_taken']} \t \t | #{user['leaves_left']} \t \t | " 
                    puts ""
                    puts "--------------------------------------------------------------------------------------------------"
                end
            end
        rescue => e 
            puts e.message
        end
    end

end