require 'mysql2'

class Connection 

    # method to establish database connection
    def self.get_connection
        begin
         Mysql2::Client.new(host: "localhost", username: "root", password: "", database: "leaves_ms_db")
        rescue => e
            puts e.message
        end
        end

    # method to close databse connection 
    def self.close_connection
        begin 
            get_connection.close
        rescue => e 
            puts e.message
        end
    end

end