# Leave Management System
### 19 September, 2019

## Introduction
This is a CLI project developed with Ruby. This application is intended to help the HR
to manage the leaves of employees. In this project,Ruby has been used as programming language and
Mysql has been used as database.
​
## Goals
To develop an application that helps to add,update list users,add leaves and show leaves taken by employee.

## Technologies Used
* Ruby Programming Language
* Mysql Database
* Git for version control
* Visual Studio Code as text editor
​
# Classes and Methods in the application
* DbConnection
* AddLeave
* DeleteLeave
* ShowIndividualUsersLeave
* ShowLeaves
* UpdateLeaves
* AddUser
* DeleteUser
* ShowUsers
* UpdateUsers
